## 🚀 Installation & Setup

##### Start the development server
```sh
npm start
```

Site is now running at `http://localhost:3000`.
