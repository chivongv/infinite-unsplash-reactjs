import React from "react";
import "./App.css";
import Images from './components/Images';

function App() {

  return (
    <div className="App">
      <div className="container">
        <Images />
      </div>
    </div>
  );
}

export default App;
