import React, { useState } from "react";
import axios from "axios";
import InfiniteScroll from "react-infinite-scroll-component";
import Image from "./Image";

const Images = () => {
  const [count, setCount] = useState(30);
  const [start, setStart] = useState(1);
  const [images, setImages] = useState([]);
  const [search, setSearch] = useState("");

  const handleChange = e => {
    setSearch(e.target.value);
  };

  const handleSubmit = e => {
    e.preventDefault();
    setStart(1);
    let url =
      search.length > 0
        ? `/api/photos?count=${count}&start=${start}&search=${search}`
        : `/api/photos?count=${count}&start=${start}`;
    try {
      axios.get(url).then(res => {
        setImages(res.data);
      });
      setStart(start + count);
    } catch (e) {
      console.log(e);
    }
  };

  const fetchImages = () => {
    setStart(start + count);
    try {
      axios
        .get(`/api/photos?count=${count}&start=${start}&search=${search}`)
        .then(res => setImages(images.concat(res.data)));
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div>
      <form>
        <h2>Infinite Unsplash Scroll</h2>
        <div>
          <input
            type="text"
            placeholder="Search something here..."
            onChange={e => handleChange(e)}
          />
          <button onClick={e => handleSubmit(e)}>Search</button>
        </div>
      </form>
      <div className="gallery">
        {images.length > 0 &&
          images.map((img, id) => <Image key={id} image={img} />)}
      </div>
      {images.length > 0 && (
        <InfiniteScroll
          dataLength={images.length}
          next={fetchImages}
          hasMore={true}
          loader={<h4>Loading...</h4>}
          endMessage={
            <p style={{ textAlign: "center" }}>
              <b>Yay! You have seen it all</b>
            </p>
          }
        />
      )}
    </div>
  );
};

export default Images;
