import React from "react";

const Image = ({ image }) => {
  return <img className="single-photo" src={image.urls.small} alt="" />;
};

export default Image;
